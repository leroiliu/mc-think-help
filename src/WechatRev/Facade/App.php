<?php

namespace MicroCyan\WechatRev\Facade;

use think\Facade;

/**
 * @method static mixed setBindKey($ChatRoomKey)
 * @method static mixed getBindValue($token)
 * @method static mixed getWxProfile($wxid)
 * @method static mixed getMessageSync($wxid)
 * @method static mixed getChatRoomMembers($wxid,$chatRoomId)
 * @method static mixed getSnsPage($wxid,$target_wxid,$type=1001,$maxId=0)
 */
class App extends Facade
{

    protected static function getFacadeClass(){
        return 'MicroCyan\WechatRev\Kernel\App';
    }

}