<?php

namespace MicroCyan\Service;

use think\Service;
use think\Validate;

class McService extends Service
{
    public function boot()
    {

        Validate::maker(function ($validate) {
            $validate->extend('checkString',function ($value,$rule=''){
                return (is_string($value)||is_numeric($value));
            },":attribute 必须为字符串或者数字类型");
            $validate->extend('checkDecimal',function ($value,$rule=''){
                return (is_numeric($value)&&($explodeArr = explode('.',$value))&&strlen($explodeArr[1]??'')==$rule);
            },':attribute 验证失败');
        });

    }


}