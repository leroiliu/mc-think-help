<?php
/**
 * Created By Leroi Liu
 * Email：675667823@qq.com
 * Date：2023/2/13
 * Description：
 **/

namespace MicroCyan\ThinkHelp\Exception;

class NeedPayException extends \Exception
{

    // 重定义构造器使 message 变为必须被指定的属性
    public function __construct($message, $code = 505, \Throwable $previous = null) {
        // 这里写用户的代码

        // 确保所有变量都被正确赋值
        parent::__construct($message, $code, $previous);
    }

}
