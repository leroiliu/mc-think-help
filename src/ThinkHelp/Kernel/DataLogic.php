<?php
namespace MicroCyan\ThinkHelp\Kernel;

/**
 * Created By Leroi Liu
 * Email：675667823@qq.com
 * Date：2023/1/29
 * Description：
 **/
class DataLogic
{


    public $msg = '';
    public $code= '';
    public $data= [];

    protected function error($msg='ERR',$code=500,$data=[]): bool
    {
        $this->code = $code;
        $this->msg  = $msg;
        $this->data = $data;
        return false;
    }
    protected function success($data=[],$msg='OK',$code=200):bool
    {
        $this->code = $code;
        $this->msg  = $msg;
        $this->data = $data;
        return true;
    }

    protected function isCheck($authName): bool
    {
        $controllerObject = nowObj();
        $action = str_replace('_','',app('request')->action(true));
        $passes = empty($controllerObject->$authName)?[]:$controllerObject->$authName;
        if (is_array($passes)){
            foreach ($passes as $key => $v){
                if (is_string($v)){
                    $passes[$key] = str_replace('_','',strtolower($v));
                }
            }
        }
        if (in_array($action,$passes)){
            return false;
        }
        return true;
    }



}
