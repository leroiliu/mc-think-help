<?php

namespace MicroCyan\ThinkHelp\Facade;

use think\Facade;

/**
 * @method static mixed SmallByOss($object,$content,$bucket='')
 */
class Upload extends Facade
{

    protected static function getFacadeClass(){
        return 'MicroCyan\ThinkHelp\Kernel\Upload';
    }

}