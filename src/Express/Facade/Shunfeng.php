<?php

namespace MicroCyan\Express\Facade;
use think\Facade;

/**
 * @method static mixed fetchRegions()
 */
class Shunfeng extends Facade
{

    protected static function getFacadeClass(){
        return 'MicroCyan\Express\Kernel\Shunfeng';
    }

}