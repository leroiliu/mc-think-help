<?php

namespace MicroCyan\MQTT\Facade;

use think\Facade;

/**
 * @method static \PhpMqtt\Client\MqttClient connect($options=[])
 */
class App extends Facade
{

    protected static function getFacadeClass(): string
    {
        return 'MicroCyan\MQTT\Kernel\App';
    }

}