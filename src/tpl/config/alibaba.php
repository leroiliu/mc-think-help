<?php
/**
 * Created By HeHeHe
 * Author：Leroi Liu
 * Email：675667823@qq.com
 * Date：2021/2/22
 * Description：
 *
 **/
$prefixOss = env('app_debug')?'oss_dev':'oss';
return [
    'oss' => [
        'accessKeyId' => env($prefixOss.'.accessKeyId', ''),
        'accessSecret' => env($prefixOss.'.accessSecret', ''),
        'bucket' => env($prefixOss.'.bucket', ''),
        'endpoint' => env($prefixOss.'.endpoint', ''),
        'url' => env($prefixOss.'.url', ''),
    ]
];
