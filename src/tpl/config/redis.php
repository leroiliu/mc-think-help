<?php

// +----------------------------------------------------------------------
// | 缓存设置
// +----------------------------------------------------------------------

$prefix = env('app_debug')?'redis_dev':'redis';

return [
    'host' => env($prefix.'.host',''),
    'port' => env($prefix.'.port',''),
    'password' => env($prefix.'.password',''),
    'defaultuse' => env($prefix.'.defaultuse','')
];
