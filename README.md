# 初始化
- 复制`tpl/config`下的文件至项目的`config`目录下，或进行覆盖
- 复制`tpl/.env.example`至项目根目录，并改名为：`.env`
- 在`app/ExceptionHandle.php`中修改`render`方法如下：
    ```php
    public function render($request, Throwable $e): Response
    {
        // 添加自定义异常处理机制
        if (get_class($e)==="MicroCyan\ThinkHelp\Exception\NotLoginException"){
            return json_error($e->getMessage(),[],501);
        }
        if (!env('app_debug')){
            return json_error($e->getMessage(),'error');
        }
        // 其他错误交给系统处理
        return parent::render($request, $e);
    }
  ```
